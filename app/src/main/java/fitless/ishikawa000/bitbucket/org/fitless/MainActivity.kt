package fitless.ishikawa000.bitbucket.org.fitless

import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.android.gms.common.api.Status
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessStatusCodes
import com.google.android.gms.fitness.data.DataPoint
import com.google.android.gms.fitness.data.DataSet
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.DataReadRequest
import com.google.android.gms.fitness.result.DataReadResult
import com.google.android.gms.fitness.result.ListSubscriptionsResult
import java.text.DateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Abandon the result,
 * for the type inference, and the type resolving
 */
fun <T> void(x: T) = Unit

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("poi", "はじまるぽいー")
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)
        this.setSupportActionBar(this.findViewById(R.id.toolbar) as Toolbar)

        GoogleApiClient.Builder(this).make(this).onConnectionEstablished() {
            Log.d("poi", "目的の処理を始めるっぽい！")
            printFitnessData(it)
        }
        Log.d("poi", "onCreate終わったっぽい！")
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
        = (item.itemId == R.id.action_settings) || super.onOptionsItemSelected(item)
}

/**
 * Connect to Fitness API.
 *
 * Try to take the read permission for Fitness API,
 * and Print debug logs to logcat with the tag of "poi" when
 * it is connected,
 * the connection is lost, is disconnected, is suspended,
 * and the connecting is failed.
 *
 * But this doesn't terminate GoogleApiClient.Builder.
 */
fun GoogleApiClient.Builder.make(context: FragmentActivity) =
    this
        .addApi(Fitness.HISTORY_API)
        .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
            override fun onConnected(bundle: Bundle?) {
                Log.d("poi", "コネクションが繋がったっぽい！")
            }
            override fun onConnectionSuspended(i: Int): Unit = when (i) {
                GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST -> void(Log.d("poi", "コネクションがロストしたっぽい"))
                GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED -> void(Log.d("poi", "コネクションが閉じたっぽい"))
                else -> void(Log.d("poi", "コネクションが…よくわかんないっぽい！: " + i))
            }
        })
        .enableAutoManage(context, 0) {
            Log.d("poi", "コネクションが作れなかったっぽい……: " + it)
        }

/**
 * Execute '(GoogleApiClient) -> Unit' when the connection is established.
 *
 * This terminates the GoogleApiClient.Builder (it depends a spec).
 *
 * This is workaround for the self reference in addConnectionCallbacks().
 * You can avoid to use a field of GoogleApiClient in the big scope
 * if you use this :D
 *
 * Google developpers should define like this if they closes GoogleApiClient.Builder x(
 */
fun GoogleApiClient.Builder.onConnectionEstablished(f: (GoogleApiClient) -> Unit): GoogleApiClient {
    class LocallyBigScope {
        var connection: GoogleApiClient? = null
        constructor(builder: GoogleApiClient.Builder) {
            this.connection = builder
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(bundle: Bundle?) = f(this@LocallyBigScope.connection as GoogleApiClient)
                    override fun onConnectionSuspended(i: Int): Unit {}
                })
                .build()
        }
    }
    return LocallyBigScope(this).connection as GoogleApiClient
}

/**
 * Main logic.
 */
fun printFitnessData(client: GoogleApiClient) = object : AsyncTask<Void?, Void?, Void?>() {
    override fun doInBackground(vararg params: Void?): Void? {
        Log.d("poi", "非同期プロセスが始まったっぽい")
        val dateFormat: DateFormat = DateFormat.getDateInstance()
        //val (now, tenDaysAgo) = atBetween(Date(), -10)
        val cal = Calendar.getInstance();
        val now = Date()
        cal.setTime(now)
        val endTime = cal.getTimeInMillis()
        cal.add(Calendar.WEEK_OF_YEAR, -1)
        val startTime = cal.getTimeInMillis()
        val readRequest: DataReadRequest = DataReadRequest.Builder()
            .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
            .bucketByActivityType(1, TimeUnit.DAYS)
            //.setTimeRange(tenDaysAgo, now, TimeUnit.MILLISECONDS)
            .setTimeRange(startTime, endTime, TimeUnit.MILLISECONDS)
            .build()

        Fitness.HistoryApi
            .readData(client, readRequest)
            .await(1, TimeUnit.MINUTES)
            .dataSets
            .flatMap { it.dataPoints }
            .forEach { x ->
                Log.d("poi", "Data point:");
                Log.d("poi", "\tType: " + x.dataType.name);
                Log.d("poi", "\tStart: " + dateFormat.format(x.getStartTime(TimeUnit.MILLISECONDS)))
                Log.d("poi", "\tEnd: " + dateFormat.format(x.getEndTime(TimeUnit.MILLISECONDS)))
                x.dataType.fields.forEach { y ->
                    Log.d("poi", "\tField: " + y.name + " Value: " + x.getValue(y))
                }
            }

        Log.d("poi", "非同期プロセスが終わるわ")
        return null
    }
}.execute()

/**
 * Return the pair of [from] and after [to] days,
 * but the pair means Long
 */
fun atBetween(from: Date, to: Int): Pair<Long, Long> {
    val cal = Calendar.getInstance()
    cal.time = from
    val base = cal.timeInMillis
    cal.add(Calendar.DATE, to)
    val after = cal.timeInMillis
    return Pair(base, after)
}
